package test;

import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;

import generic.Basetest;
import pom.Loginpage;

public class Sirinadu extends Basetest {
	@Test(priority=1)
	public void testLogin() throws InterruptedException, IOException
	{	
		Reporter.log("Scenario : Elements present in Login page are: ",true);	
		Loginpage l=new Loginpage(driver);
		l.verifyEMail();
		l.verifypwd();
		l.verifyLoginButton();	
		l.verifyForgotPassword();
	
		Reporter.log("\nScenario : Enter invalid Username and invalid Password",true);
		l.setEmail("Invalid");
		l.setPassword("Invalid");
		l.clickLogin();
		
		Reporter.log("\nScenario : Enter invalid Username and valid Password",true);
		l.setEMail("Invalid");
		l.setPassword("Novaders@321");
		l.clickLogin();
		
		Reporter.log("\nScenario : Enter valid Username and invalid Password",true);
		l.setEMail("aravind@novaders.com");
		l.setPassword("Invalid");
		l.clickLogin();
		
		Reporter.log("\nScenario : Leave Username and Password blank and click Login button",true);
		driver.navigate().refresh();
		l.setEMail("");
		l.setPassword("");
		l.clickLogin();
		
		Reporter.log("\nScenario : Enter valid Username and invalid Password",true);
		l.setEMail("aravind@novaders.com");
		l.setPassword("Novaders@321");
		l.clickLogin();
	}

}
