package generic;

public interface Autoconstant {
	String chromekey="webdriver.chrome.driver";
	String geckokey="webdriver.gecko.driver";
	String chromevalue="./driver/chromedriver.exe";
	String geckovalue="./driver/geckodriver.exe";
	String inputpath="./data/input.xlsx";

}
