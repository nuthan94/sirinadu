package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import org.testng.Assert;
import org.testng.Assert;
import org.testng.Reporter;

import generic.Basepage;

public class Dashboard extends Basepage {

//	private static final String Beauty = null;
	public Dashboard(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//a[@href='#/dashboard']")
	  private WebElement dashboard;                                     //Dashboard
	
	@FindBy(xpath="//div[@class='card-footer']")
	 private WebElement cardfooter;                                     //click on category Cardfooter
	
	@FindBy(xpath="//input[@placeholder='Search']")
	private WebElement  srchcategory;                                   //Search category
	
	@FindBy(xpath="/html/body/app-dashboard/div/main/div/app-categorylist/div/div[2]/div[1]/i")
	private   WebElement  CatsrchCancel;
	
	@FindBy(xpath="//button[@type='button']")
	private WebElement Addvideos;                                        //Add videos
	
	@FindBy(xpath="//button[@class='btn btn-primary addCatButton']")
	 private WebElement Addcategory;                                      //Add category
	
	@FindBy(xpath="//input[@name='categoryName']")
	private WebElement Catname;                                           //Enter category name
	
	@FindBy(xpath="//input[@placeholder='Category System Name']")
	private WebElement SystemName;                                        //Category System Name
	 
	@FindBy(xpath="//textarea[@name='description']")
	private WebElement Description;                                       //Description
	
	@FindBy(xpath="//div[@class='c-btn']")
	private WebElement Accesslist;
	
	WebDriverWait wait=new WebDriverWait(driver, 10);
	public void clickCatSearch(String CatName )
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(srchcategory));
			srchcategory.sendKeys(CatName);
			srchcategory.click();
	           Thread.sleep(5000);
	          Reporter.log(CatName+" created successfully",true);
	           CatsrchCancel.click();
	
	  }
	  catch(Exception e) {
		Assert.fail();
	}
	}
}

			
		
		
		
	
	
	
	
	
	
	
     

