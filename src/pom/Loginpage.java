package pom;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import generic.Basepage;

public class Loginpage extends Basepage
{

	public Loginpage(WebDriver driver)
	{
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//input[@placeholder='Email']")
	private WebElement EMail;
	
	@FindBy(xpath="//input[@placeholder='Password']")
	private WebElement password;
	
	@FindBy(xpath="//button[.='Login']")
	private WebElement loginBtn;
	
	@FindBy(xpath="/html/body/app-dashboard/ng-component/div/div/div/div/div/div/div/form/div[3]/div[2]/span")
	private WebElement errorMSG;
	
	@FindBy(xpath="//span[text()='Forgot Password? ']")
	private WebElement forgotPassword;
	
	
	WebDriverWait wait=new WebDriverWait(driver, 10);
	public void setEmail(String email) 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(EMail));
			EMail.clear();
			EMail.sendKeys(email);	
			Reporter.log("Email: "+email,true);
		}
		catch(Exception e)
		{
        
	    Reporter.log("Email textline not displayed",true);
			Assert.fail();
		}
	}
 
	public void setPassword(String pwd)
	{
	 try{
		 wait.until(ExpectedConditions.visibilityOf(password));
		 password.clear();
		 password.sendKeys(pwd);
		 Reporter.log("Password: "+pwd,true);
	 }
	 catch(Exception e)
	 {
		Reporter.log ("Password textline not displayed",true);
		 Assert.fail();
	 }
		 
 }
	
	public void clickLogin()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(loginBtn));
			loginBtn.click();	
			wait.until(ExpectedConditions.urlToBe("https://www.sirinadu.com/#/pages/login"));
			Reporter.log("Logged in Successfully",true);
		}
		catch (Exception e) 
		{
				String msg=errorMSG.getText();
				Reporter.log("ERROR MESSAGE: "+msg,true);
			
		}
	}

	public void verifyEMail() 
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(EMail));
			Reporter.log(EMail.getAttribute("placeholder"),true);
		}
		catch(Exception e)
		{
			Reporter.log("EMail not present",true);
		}
		
	}

	public void verifypwd()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(password));
			Reporter.log(password.getAttribute("placeholder"),true);
		}
		catch(Exception e)
		{
			Reporter.log("password not present",true);
		}
		
	}

	public void verifyLoginButton()
	{
		try{
			wait.until(ExpectedConditions.visibilityOf(loginBtn));
			Reporter.log(loginBtn.getText(),true);
		}
		catch(Exception e)
		{
			Reporter.log("loginBtn not present",true);
		}
	}

	public void setEnumber(String string) {
		
		
	}

	public void verifyForgotPassword() 
	{
		try{
		wait.until(ExpectedConditions.visibilityOf(forgotPassword));
		Reporter.log(forgotPassword.getText(),true);
		}
	catch(Exception e)
	{
		Reporter.log("forgotPassword not present",true);
	}	
	}

	public void setEMail(String string) {
		// TODO Auto-generated method stub
		
	}

	
		
	}

 
		




